<?php
// include the Ada framework
require_once('ada/framework.php');
// actually run it
$ada = new Ada();
// print the first part of the template
$ada->printPartialFromConfig('templates.before');

// place your own php code here
echo <<<EOD
<section>
	<div class="container">
EOD;

echo '<div class="row col-12">This is the index page...</div>';

echo <<<EOD
	</div>
</section>
EOD;

// print the last part of the template
$ada->printPartialFromConfig('templates.after');
?>

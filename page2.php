<?php
// include the Ada framework
require_once('ada/framework.php');
// actually run it
$ada = new Ada();
// print the first part of the template
$ada->printPartialFromConfig('templates.before');

// place your own php code here
echo <<<EOD
<section>
	<div class="container">
EOD;

// you can also use the ADA templating mechanism within you own code
echo $ada->getTwigTemplate('<div class="row col-12">You are on {{currentPage.name}} (<-- this comes from a variable)</div>', $ada->getContext());

echo <<<EOD
	</div>
</section>
EOD;

// print the last part of the template
$ada->printPartialFromConfig('templates.after');
?>
